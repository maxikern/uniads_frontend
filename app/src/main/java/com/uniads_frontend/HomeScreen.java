package com.uniads_frontend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeScreen extends AppCompatActivity {

    private Button kategorieButton;
    private Button profileButton;
    private Button inserierenButton;


    /*
    private class LoginButtonListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {

                Intent intent = new Intent(HomeScreen.this, LoginActivity.class);
                startActivity(intent);

        }
    }
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);


        kategorieButton = (Button) findViewById(R.id.KategorieButton);
        profileButton = (Button) findViewById(R.id.profileButton);
        inserierenButton = (Button) findViewById(R.id.inserierenButton);

        kategorieButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onKategorieClick();
            }

        });
        profileButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onProfileClick();
            }

        });

        inserierenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                onInserierenClick();
            }
        });
    }


    private void onKategorieClick(){
        Intent intent = new Intent (HomeScreen.this, KategorieActivity.class);
        startActivity(intent);
    }
    private void onProfileClick(){
        Intent intent = new Intent (HomeScreen.this, ProfileActivity.class);
        startActivity(intent);
    }

    private void onInserierenClick(){
        Intent intent = new Intent(HomeScreen.this, InserierenActivity.class );
        startActivity(intent);
    }


}

