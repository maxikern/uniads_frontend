package com.uniads_frontend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class KategorieActivity extends AppCompatActivity {

    private Button bookButton;
    private Button groupButton;
    private Button driveButton;
    private Button nhButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorie);

        bookButton = (Button) findViewById(R.id.bookButton);
        groupButton = (Button) findViewById(R.id.groupButton);
        driveButton = (Button) findViewById(R.id.driveButton);
        nhButton = (Button) findViewById(R.id.nhButton);

        bookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBookClick();
            }
        });

        groupButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onGroupClick();
            }
        });

        driveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onDriveClick();
            }
        });

        nhButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                onNhClick();
            }
        });
    }




    private void onBookClick () {
        Intent intent = new Intent(KategorieActivity.this, BookActivity.class);
        startActivity(intent);
    }
    private void onGroupClick(){
        Intent intent = new Intent (KategorieActivity.this, LerngruppeActivity.class);
        startActivity(intent);
    }

    private void onDriveClick(){
        Intent intent = new Intent (KategorieActivity.this, FahrgemeinschaftActivity.class);
        startActivity(intent);
    }

    private void onNhClick(){
        Intent intent = new Intent (KategorieActivity.this, NachhilfeActivity.class);
        startActivity(intent);
    }
}
