package com.uniads_frontend;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NachhilfeActivity extends AppCompatActivity {

    //variablen für controls siehe unten
    //private Button bookButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nachhilfe);
        //Control Variablen zuweisen
        //bookButton = (Button) findViewById(R.id.bookButton);

        //Events zu Controls zuweisen
    }

    @Override
    protected void onStart() {
        super.onStart();
        //hier Controls füllen aus Backend
    }
}
